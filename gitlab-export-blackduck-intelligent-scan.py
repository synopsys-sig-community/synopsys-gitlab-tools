import argparse
import glob
import hashlib
import json
import os
import random
import re
import shutil
import sys
import zipfile

from blackduck import Client

# Parse command line arguments
parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                description='Generate GitLab dependency scan data from Black Duck Intelligent Scan')
parser.add_argument('--debug', default=0, help='set debug level [0-9]')
parser.add_argument('--url', required=True, help='Black Duck Base URL')
parser.add_argument('--token', required=True, help='Black Duck API Token')
parser.add_argument('--output', required=True, help='Detect output directory')

args = parser.parse_args()

debug = args.debug
bd_apitoken = args.token
bd_url = args.url
bd_output_dir = args.output

bd = Client(token=bd_apitoken,
        base_url=bd_url,
        timeout=300)

bd_output_status_glob = glob.glob(bd_output_dir + "/runs/*/status/status.json")
if (len(bd_output_status_glob) == 0):
    print("ERROR: Unable to find output scan files in: " + bd_output_dir + "/runs/*/status/status.json")
    sys.exit(1)

bd_output_status = bd_output_status_glob[0]

print("INFO: Parsing Black Duck Scan output from " + bd_output_status)
with open(bd_output_status) as f:
    output_status_data = json.load(f)

detected_package_files = []
for detector in output_status_data['detectors']:
    # Reverse order so that we get the priority from detect
    for explanation in reversed(detector['explanations']):
        if (str.startswith(explanation, "Found file: ")):
            package_file = explanation[len("Found file: "):]
            if (os.path.isfile(package_file)):
                detected_package_files.append(package_file)
                if (debug): print(f"DEBUG: Explanation: {explanation} File: {package_file}")


# Find project name and version to use in looking up baseline data
project_baseline_name = output_status_data['projectName']
project_baseline_version = output_status_data['projectVersion']

print(f"INFO: Running for project '{project_baseline_name}' version '{project_baseline_version}'")
