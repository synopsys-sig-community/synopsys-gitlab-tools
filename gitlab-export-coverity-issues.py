#!/usr/bin/python3

import json
import linecache
import sys
import os
import argparse
import urllib
import glob

from urllib.parse import urlparse
from wscoverity import WebServiceClient, ConfigServiceClient, DefectServiceClient
import ssl

# Parse command line arguments
parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
        description='Post Coverity issue summary to GitLab CI Notes Object')
parser.add_argument('--debug', default=0, help='set debug level [0-9]')
parser.add_argument('--coverity-json', required=True, help='Coverity Json V7 output')
parser.add_argument('--markdown-format', action='store_true', default=False)
parser.add_argument('--url', help='Coverity Server URL')
parser.add_argument('--stream', required=True, help='Coverity stream name')
parser.add_argument('--project', required=True, help='Coverity project name')


args = parser.parse_args()

debug = int(args.debug)
coverity_json = args.coverity_json
markdown_format = args.markdown_format
url = args.url
stream = args.stream
project = args.project

coverity_username = os.getenv("COV_USER")
coverity_passphrase = os.getenv("COVERITY_PASSPHRASE")

if (coverity_username == None or coverity_passphrase == None):
    print(f"ERROR: Must specificy COV_USER and COVERITY_PASSPHRASE environment variables")
    sys.exit(1)

o = urlparse(args.url)
host = o.hostname
port = str(o.port)
scheme = o.scheme
if scheme == "https":
    do_ssl = True
    port = "443"
else:
    do_ssl = False

# TODO Properly handle self-signed certificates, but this is challenging in Python
try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    # Legacy Python that doesn't verify HTTPS certificates by default
    pass
else:
    # Handle target environment that doesn't support HTTPS verification
    ssl._create_default_https_context = _create_unverified_https_context

print(f"DEBUG: Connect to host={host} port={port}")
defectServiceClient = DefectServiceClient(host, port, do_ssl, coverity_username, coverity_passphrase)

mergedDefectDOs = defectServiceClient.get_merged_defects_for_stream(stream)

# Look for merge keys seen in reference stream - but ignore if they are "Fixed" since we will want to include
# them if they are re-introduced
merge_keys_seen_in_ref = dict()
for md in mergedDefectDOs:
    for dsa in md.defectStateAttributeValues:
        adId = dsa.attributeDefinitionId
        if (adId.name == "DefectStatus"):
            advId = dsa.attributeValueId
            if (advId.name != "Fixed"):
                merge_keys_seen_in_ref[md.mergeKey] = md.cid

if debug: print(f"DEBUG: merge_keys_seen_in_ref={merge_keys_seen_in_ref}")

with open(coverity_json) as f:
    data = json.load(f)

print(f"INFO: Reading incremental analysis results from {coverity_json}")
if debug: print(f"DEBUG: {json.dumps(data, indent = 4, sort_keys=True)}")

sast_report = dict()
sast_report["version"] = "2.0"
vulnerabilities = []

for item in data["issues"]:
    checkerName = item["checkerName"]
    checkerProperties = item["checkerProperties"]
    subcategoryShortDescription = checkerProperties["subcategoryShortDescription"]
    subcategoryLongDescription = checkerProperties["subcategoryLongDescription"]
    cwe = checkerProperties["cweCategory"]
    impact = checkerProperties["impact"]
    codeLangauge = item["code-language"]
    mergeKey = item["mergeKey"]
    strippedMainEventFilePathname = item["strippedMainEventFilePathname"]
    mainEventLineNumber = item["mainEventLineNumber"]

    print(f"DEBUG: Looking for {mergeKey} in: {merge_keys_seen_in_ref}")
    if merge_keys_seen_in_ref[mergeKey]:
        cid_url = f"{url}/query/defects.htm?project={project}&cid={merge_keys_seen_in_ref[mergeKey]}"
    else:
        cid_url = ""

    eventNumber = 1
    if debug: print(f"DEBUG: Found merge key={mergeKey}")
    newIssue = dict()
    newIssue["id"] = mergeKey
    newIssue["category"] = "sast"
    newIssue["name"] = subcategoryShortDescription
    newIssue["message"] = f"{subcategoryShortDescription}"
    newIssue["description"] = f"{subcategoryLongDescription}"
    newIssue["severity"] = impact
    newIssue["confidence"] = "Medium"
    scanner = dict()
    scanner["id"] = "synopsys_coverity"
    scanner["name"] = "Synopsys Coverity"
    newIssue["scanner"] = scanner
    location = dict()

    # Build map of lines
    event_tree_lines = dict()
    event_tree_events = dict()
    for event in item["events"]:
        event_file = event['strippedFilePathname']
        event_line = int(event['lineNumber'])

        if event_file not in event_tree_lines:
            event_tree_lines[event_file] = dict()
            event_tree_events[event_file] = dict()

        event_line_start = event_line - 3
        if event_line_start < 0: event_line_start = 0
        event_line_end = event_line + 3
        for i in range(event_line_start, event_line_end):
            event_tree_lines[event_file][i] = 1

        if event_line not in event_tree_events[event_file]:
            event_tree_events[event_file][event_line] = []

        event_tree_events[event_file][event_line].append(f"{event['eventNumber']}. {event['eventTag']}: {event['eventDescription']}")

    if debug: print(f"DEBUG: event_tree_lines={event_tree_lines}")
    if debug: print(f"DEBUG: event_tree_events={event_tree_events}")

    defect_body = ""
    for filename in event_tree_lines.keys():
        defect_body += f"**From {filename}:**\n"

        defect_body += "```\n"
        for i in event_tree_lines[filename].keys():
            if (i in event_tree_events[filename]):
                for event_str in event_tree_events[filename][i]:
                    defect_body += f"{event_str}\n"

            code_line = linecache.getline(filename, i)
            defect_body += f"%5d {code_line}" % i

        defect_body += "```\n"

    if debug: print(f"DEBUG: Defect body={defect_body}")

    for event in item["events"]:
        if event["main"]:
            location["file"] = event["strippedFilePathname"]
            location["start_line"] = event["lineNumber"]
            location["end_line"] = event["lineNumber"]
            #if (len(item["functionDisplayName"].rsplit('.', 1)) > 0):
            #    class_name = item["functionDisplayName"].rsplit('.', 1)[0]
            #    method_name = item["functionDisplayName"].rsplit('.', 1)[1]
            #else:
            class_name = item["functionDisplayName"]
            method_name = item["functionDisplayName"]
            location["class"] = class_name
            location["method"] = method_name

            newIssue["description"] = f"{newIssue['description']} {event['eventDescription']}"
        if event["remediation"]:
            newIssue["description"] = newIssue["description"] + "\n\n" + event["eventDescription"]
            newIssue["solution"] = event["eventDescription"]

    if markdown_format:
        newIssue["description"] += f"\n\n{defect_body}"

    newIssue["location"] = location

    identifiers = []
    identifiers_snps = dict()
    identifiers_snps["type"] = "synopsys_coverity_type"
    identifiers_snps["name"] = "Synopsys Coverity-" + checkerName
    identifiers_snps["value"] = checkerName
    identifiers_snps["url"] = cid_url

    identifiers.append(identifiers_snps)
    identifiers_cwe = dict()
    identifiers_cwe["type"] = "cwe"
    identifiers_cwe["name"] = "CWE-" + cwe
    identifiers_cwe["value"] = cwe
    identifiers_cwe["url"] = "https://cwe.mitre.org/data/definitions/" + cwe + ".html"
    identifiers.append(identifiers_cwe)

    newIssue["identifiers"] = identifiers

    vulnerabilities.append(newIssue)

sast_report["vulnerabilities"] = vulnerabilities

print(f"INFO: Writing to synopsys-gitlab-sast.json")
with open('synopsys-gitlab-sast.json', 'w') as fp:
  json.dump(sast_report, fp, indent=4)