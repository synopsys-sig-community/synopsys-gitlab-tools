import argparse
import glob
import hashlib
import json
import os
import random
import re
import shutil
import sys
import zipfile
from BlackDuckUtils import BlackDuckOutput as bo
from BlackDuckUtils import Utils as bu
from BlackDuckUtils import bdio as bdio
from BlackDuckUtils import globals as bdglobals
import gitlab as gitlab
import globals

from blackduck import Client

# Parse command line arguments
parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                description='Generate GitLab dependency scan data from Black Duck Intelligent Scan')
parser.add_argument('--debug', default=0, help='set debug level [0-9]')
parser.add_argument('--url', required=True, help='Black Duck Base URL')
parser.add_argument('--token', required=True, help='Black Duck API Token')
parser.add_argument('--output', required=True, help='Detect output directory')
parser.add_argument('--upgrade_policy', default="short_term", help='Detect output directory')
parser.add_argument('--comment_on_mr', default="false", help='Detect output directory')
parser.add_argument('--gl-token', required=True, help='GitLab Private Token')

args = parser.parse_args()

globals.debug = args.debug
bdglobals.debug = args.debug
bd_apitoken = args.token
bd_url = args.url
bd_output_dir = args.output
upgrade_policy = args.upgrade_policy
comment_on_mr = args.comment_on_mr
gl_token = args.gl_token

bd = Client(token=bd_apitoken,
        base_url=bd_url,
        timeout=300)

project_baseline_name, project_baseline_version, detected_package_files = bo.get_blackduck_status(bd_output_dir)

print(f"INFO: Running for project '{project_baseline_name}' version '{project_baseline_version}'")

bdio_graph, bdio_projects = bdio.get_bdio_dependency_graph(bd_output_dir)

if (len(bdio_projects) == 0):
    print("ERROR: Unable to find base project in BDIO file")
    sys.exit(1)

rapid_scan_data = bo.get_rapid_scan_results(bd_output_dir, bd)

dependency_report = dict()
dependency_report["version"] = "2.0"
vulnerabilities = []
remediations = []

merge_request_comment_lines = []

for item in rapid_scan_data['items']:
    if (globals.debug):
        print(f"DEBUG: Component: {item['componentIdentifier']}")
        print(item)
        #sys.exit(1)

    comp_ns, comp_name, comp_version = bu.parse_component_id(item['componentIdentifier'])

    # Track the root dependencies
    dependency_paths = []
    direct_ancestors = dict()

    if (globals.debug): print(f"DEBUG: Looking for {item['componentIdentifier']} in BDIO graph")
    if (globals.debug):
        print(f"DEBUG: comp_ns={comp_ns} comp_name={comp_name} comp_version={comp_version}")

    dependency_type = bdio.get_dependency_type(bdio_graph, bdio_projects, item['componentIdentifier'])
    if (globals.debug): print(f"DEBUG: type is: {dependency_type}")

    shortTerm, longTerm = bu.get_upgrade_guidance(bd, item['componentIdentifier'])

    if (globals.debug): print(f"DEBUG: Short term upgrade guidance={shortTerm} and Long term upgrade guidance={longTerm}")

    # TODO Offer option for short or long term upgrade
    if (upgrade_policy == "long_term"):
        if (longTerm != None): upgrade_version = longTerm
        else: upgrade_version = shortTerm
    else:
        upgrade_version = shortTerm

    package_file, package_line = bu.detect_package_file(detected_package_files, item['componentIdentifier'])

    if (globals.debug): print(f"DEBUG: package file for {item['componentIdentifier']} is {package_file} on line {package_line} type is {dependency_type}")

    for vuln in item['policyViolationVulnerabilities']:
        if (globals.debug): print(f"DEBUG: Vulnerability basic data: " + json.dumps(vuln, indent=4))
        vuln_data = bd.get_json(vuln['_meta']['href'])
        if (globals.debug): print(f"DEBUG: Vulnerability extended data: " + json.dumps(vuln_data, indent=4))

        comment_on_pr = ""
        if upgrade_version is not None:
            comment_on_pr += f"| {comp_name} | {vuln['name']} |  {vuln['vulnSeverity']} | {vuln['violatingPolicies'][0]['policyName']} | {vuln['description']} | {comp_version} | {upgrade_version} |"
        else:
            comment_on_pr += f"| {comp_name} | {vuln['name']} |  {vuln['vulnSeverity']} | {vuln['violatingPolicies'][0]['policyName']} | {vuln['description']} | {comp_version} | N/A |"
        merge_request_comment_lines.append(comment_on_pr)

        new_vuln = dict()
        new_vuln["id"] = vuln["name"]
        new_vuln["category"] = "dependency_scanning"
        new_vuln["name"] = vuln["name"]
        new_vuln["message"] = vuln["name"] + " in " + comp_name
        new_vuln["description"] = vuln["description"]
        new_vuln["severity"] = vuln["vulnSeverity"]
        if (upgrade_version != None):
            new_vuln["solution"] = f"Upgrade to version {upgrade_version}"
        else:
            new_vuln["solution"] = f"No upgrade guidance available"
        new_vuln["scanner"] = { "id" : "blackduck", "name" : "Synopsys Black Duck" }
        new_vuln["location"] = {
            "file" : package_file,
            "dependency" : {
                "package" : {
                    "name" : comp_name
                },
                "version" : comp_version
            }
        }
        vuln_link = "N/A"
        if (vuln_data["source"] == "NVD"):
            vuln_type = "cve"
            for link in vuln_data["_meta"]["links"]:
                if (link["rel"] == "nist"):
                    vuln_link = link["href"]
        elif (vuln_data["source"] == "BDSA"):
            vuln_type = "bdsa"
            vuln_link = vuln_data["_meta"]["href"]
            if "solution" in vuln_data:
                new_vuln["solution"] = vuln_data["solution"]
        else:
            vuln_type = vuln_data["source"]
            print(f"ERROR: Unknown vulnerability source: {vuln_type}")
            sys.exit(1)
        new_vuln["identifiers"] = [ {
            "type" : vuln_type,
            "name" : vuln["name"],
            "value" : vuln["name"],
            "url" : vuln_link
        }]
        links = []
        for link in vuln_data["_meta"]["links"]:
            links.append({ "url" : link["href"]})
        new_vuln["links"] = links

        vulnerabilities.append(new_vuln)

dependency_report["vulnerabilities"] = vulnerabilities
dependency_report["remediations"] = remediations

with open('synopsys-gitlab-dependency.json', 'w') as fp:
  json.dump(dependency_report, fp, indent=4)

if (comment_on_mr == "true"):
    if (globals.debug): print(f"DEBUG: Comment on MR")

    merge_request_id = int(os.getenv('CI_MERGE_REQUEST_ID'))
    if (globals.debug): print(f"DEBUG: Merge_request_id = {merge_request_id}")

    ci_api_url = os.getenv("CI_API_V4_URL")
    if (globals.debug): print(f"DEBUG: CI_API_V4_URL = {ci_api_url}")

    ci_job_token = os.getenv("CI_JOB_TOKEN")
    if (globals.debug): print(f"DEBUG: CI_JOB_TOKEN = {ci_job_token}")
    #gl = gitlab.Gitlab("https://gitlab.com", job_token=ci_job_token)
    ci_server_url = os.getenv("CI_SERVER_URL")
    if (globals.debug): print(f"DEBUG: CI_SERVER_URL = {ci_server_url}")
    gl = gitlab.Gitlab(ci_server_url, private_token=gl_token)
    # glpat-WaEXXT6WG4i8qStKeDhr

    ci_project_namespace = os.getenv("CI_PROJECT_NAMESPACE")
    ci_project_name = os.getenv("CI_PROJECT_NAME")
    if (globals.debug): print(f"DEBUG: CI_PROJECT_NAMESPACE = {ci_project_namespace} CI_PROJECT_NAME = {ci_project_name}")

    project_name_with_namespace = ci_project_namespace + "/" + ci_project_name
    if (globals.debug): print(f"Look up {project_name_with_namespace}")

    project = gl.projects.get(project_name_with_namespace)
    if (globals.debug): print(project)

    mrs = project.mergerequests.list()
    if (globals.debug):
        print("DEBUG: MRs=")
        print(mrs)
    for mr in mrs:
        print("MR:")
        print(mr)
        if (mr.id == int(merge_request_id)):
            print(f"DEBUG: Found MR #{merge_request_id}")
            break

    body = f'''
Synopsys Black Duck found the following vulnerabilities in Merge Request #{merge_request_id}:

'''
    body += "| Component | Vulnerability | Severity | Policy | Description | Version | Upgrade To |\n"
    body += "| --- | --- | --- | --- | --- | --- | --- |\n"

    body += "\n".join(merge_request_comment_lines) + "\n\n"

    if globals.debug:
        print("DEBUG: Attempting to create discussion. MR=")
        print(mr)

    mr_note = mr.notes.create({'body': body})
