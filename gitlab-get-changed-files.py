#!/usr/bin/python

import json
import os
import argparse
import gitlab as gitlab

truncate_security_results = True

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
        description='Post Coverity issue summary to GitHub SARIF')
parser.add_argument('--debug', default=0, help='set debug level [0-9]')
parser.add_argument('--output', required=True, help='output file')

args = parser.parse_args()

debug = int(args.debug)
output = args.output

merge_request_id = int(os.getenv('CI_MERGE_REQUEST_ID'))
if (debug): print(f"DEBUG: Merge_request_id = {merge_request_id}")

ci_api_url = os.getenv("CI_API_V4_URL")
if (debug): print(f"DEBUG: CI_API_V4_URL = {ci_api_url}")

ci_job_token = os.getenv("CI_JOB_TOKEN")
if (debug): print(f"DEBUG: CI_JOB_TOKEN = {ci_job_token}")

gitlab_token = os.getenv("GITLAB_TOKEN")
if (debug): print(f"DEBUG: GITLAB_TOKEN = {gitlab_token}")

# gl = gitlab.Gitlab("https://gitlab.com", job_token=ci_job_token)
ci_server_url = os.getenv("CI_SERVER_URL")
if (debug): print(f"DEBUG: CI_SERVER_URL = {ci_server_url}")

if (gitlab_token != None):
    gl = gitlab.Gitlab(ci_server_url, private_token=gitlab_token)
else:
    gl = gitlab.Gitlab(ci_server_url, job_token=ci_job_token)

ci_project_namespace = os.getenv("CI_PROJECT_NAMESPACE")
ci_project_name = os.getenv("CI_PROJECT_NAME")
if (debug): print(f"DEBUG: CI_PROJECT_NAMESPACE = {ci_project_namespace} CI_PROJECT_NAME = {ci_project_name}")

project_name_with_namespace = ci_project_namespace + "/" + ci_project_name
if (debug): print(f"Look up {project_name_with_namespace}")

project = gl.projects.get(project_name_with_namespace)
if (debug): print(f"DEBUG: Gitlab project={project}")

mrs = project.mergerequests.list()
if (debug):
    print("DEBUG: MRs=")
    print(mrs)
for mr in mrs:
    print("DEBUG: MR:")
    print(mr)
    if (mr.id == int(merge_request_id)):
        print(f"DEBUG: Found MR #{merge_request_id}")
        break

changes = mr.changes()
if debug: print(f"DEBUG: mr.changes()={json.dumps(changes, indent=4)}")

file_changes = dict()
with open(output, "w") as fp:
    for change in changes['changes']:
        if debug: print(f"DEBUG: change={change}")
        filename = change['new_path']
        if debug: print(f"DEBUG: filename={filename}")
        fp.write(f"{filename}\n")

fp.close()