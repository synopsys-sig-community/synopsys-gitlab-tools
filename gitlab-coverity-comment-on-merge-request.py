#!/usr/bin/python

import json
import subprocess
import sys
import os
import argparse
import ssl
import re
import linecache
import gitlab as gitlab

# Parse command line arguments
from os.path import exists

from urllib.parse import urlparse
from wscoverity import WebServiceClient, ConfigServiceClient, DefectServiceClient

def find_ref_for_line(filepath, line):
    line_range = str(line) + ',' + str(line)

    if (not exists(filepath)):
        print(f"WARNING: File '{filepath}' does not exist")
        return None

    try:
        output = subprocess.check_output(['git', 'blame', '-l', '-L', line_range, filepath])
    except subprocess.CalledProcessError as grepexc:
        print(f"WARNING: Git blame failed: {grepexc}")
        return None, None

    for line in output.splitlines():
        # 9419f31f2c6878e8b29370a73b1d96bd3f69d6f2 (James Croall 2022-01-09 14:26:41 +0000 21) var req = https.request({port: 1336, host: 'https://example2.com', rejectUnauthorized: false}, function(){
        line = line.decode("utf-8")
        sline = line.split(' ')
        return sline[0]

    return None


def get_lines_from_file(filename, start_line, end_line):
    ret_lines = dict()

    with open(filename, 'r') as fp:
        # lines to read
        line_numbers = range(start_line, end_line)
        # To store lines
        lines = []
        for i, line in enumerate(fp):
            # read line 4 and 7
            if i in line_numbers:
                ret_lines[i] = line.strip()
            elif i > end_line:
                # don't read after line 7 to save time
                break

    return ret_lines


truncate_security_results = True

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
        description='Post Coverity issue summary to GitHub SARIF')
parser.add_argument('--debug', default=0, help='set debug level [0-9]')
parser.add_argument('--coverity-json', required=True, help='Coverity JSON output')
parser.add_argument('--sigma-json', required=True, help='Sigma JSON output')
parser.add_argument('--url', required=True, help='Coverity Connect URL')
parser.add_argument('--stream', required=True, help='Coverity stream name for reference')

args = parser.parse_args()

debug = int(args.debug)
coverity_json = args.coverity_json
sigma_json = args.sigma_json
url = args.url
stream = args.stream

coverity_username = os.getenv("COV_USER")
coverity_passphrase = os.getenv("COVERITY_PASSPHRASE")

if (coverity_username == None or coverity_passphrase == None):
    print(f"ERROR: Must specificy COV_USER and COVERITY_PASSPHRASE environment variables")
    sys.exit(1)

o = urlparse(args.url)
host = o.hostname
port = str(o.port)
scheme = o.scheme
if scheme == "https":
    do_ssl = True
    port = "443"
else:
    do_ssl = False

# TODO Properly handle self-signed certificates, but this is challenging in Python
try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    # Legacy Python that doesn't verify HTTPS certificates by default
    pass
else:
    # Handle target environment that doesn't support HTTPS verification
    ssl._create_default_https_context = _create_unverified_https_context

print(f"DEBUG: Connect to host={host} port={port}")
defectServiceClient = DefectServiceClient(host, port, do_ssl, coverity_username, coverity_passphrase)

mergedDefectDOs = defectServiceClient.get_merged_defects_for_stream(stream)

# Look for merge keys seen in reference stream - but ignore if they are "Fixed" since we will want to include
# them if they are re-introduced
merge_keys_seen_in_ref = dict()
for md in mergedDefectDOs:
    for dsa in md.defectStateAttributeValues:
        adId = dsa.attributeDefinitionId
        if (adId.name == "DefectStatus"):
            advId = dsa.attributeValueId
            if (advId.name != "Fixed"):
                merge_keys_seen_in_ref[md.mergeKey] = 1

if debug: print(f"DEBUG: merge_keys_seen_in_ref={merge_keys_seen_in_ref}")

total_issues_commented = 0

# Process output from Coverity
with open(coverity_json, encoding='utf-8') as f:
  data = json.load(f)

print(f"INFO: Reading incremental analysis results from {coverity_json}")
if(debug): print("DEBUG: " + json.dumps(data, indent = 4, sort_keys=True) + "\n")

local_issues = data['issues']
issues_to_report = dict()
issues_to_comment_on = []
for issue in local_issues:
    # TODO For Demo Day
    if "SIGMA." in issue['checkerName']: continue
    if issue['mainEventLineNumber'] == 8: continue
    if issue['mergeKey'] in merge_keys_seen_in_ref:
        if debug: print(f"DEBUG: merge key {issue['mergeKey']} seen in reference stream, file={issue['strippedMainEventFilePathname']}")
    else:
        if debug: print(f"DEBUG: merge key {issue['mergeKey']} NOT seen in reference stream, file={issue['strippedMainEventFilePathname']}")
        issues_to_report[issue['mergeKey']] = issue
        issues_to_comment_on.append(issue)

if debug: print(f"DEBUG: Issues to report: {issues_to_report}")

merge_request_id = int(os.getenv('CI_MERGE_REQUEST_ID'))
if (debug): print(f"DEBUG: Merge_request_id = {merge_request_id}")

ci_api_url = os.getenv("CI_API_V4_URL")
if (debug): print(f"DEBUG: CI_API_V4_URL = {ci_api_url}")

ci_job_token = os.getenv("CI_JOB_TOKEN")
if (debug): print(f"DEBUG: CI_JOB_TOKEN = {ci_job_token}")

gitlab_token = os.getenv("GITLAB_TOKEN")
if (debug): print(f"DEBUG: GITLAB_TOKEN = {gitlab_token}")

# gl = gitlab.Gitlab("https://gitlab.com", job_token=ci_job_token)
ci_server_url = os.getenv("CI_SERVER_URL")
if (debug): print(f"DEBUG: CI_SERVER_URL = {ci_server_url}")

if (gitlab_token != None):
    gl = gitlab.Gitlab(ci_server_url, private_token=gitlab_token)
else:
    gl = gitlab.Gitlab(ci_server_url, job_token=ci_job_token)

ci_project_namespace = os.getenv("CI_PROJECT_NAMESPACE")
ci_project_name = os.getenv("CI_PROJECT_NAME")
if (debug): print(f"DEBUG: CI_PROJECT_NAMESPACE = {ci_project_namespace} CI_PROJECT_NAME = {ci_project_name}")

project_name_with_namespace = ci_project_namespace + "/" + ci_project_name
if (debug): print(f"Look up {project_name_with_namespace}")

project = gl.projects.get(project_name_with_namespace)
if (debug): print(f"DEBUG: Gitlab project={project}")

# Can't get direct by Merge Request ID for some reason!
#if debug: print(f"DEBUG: Get MR ID={merge_request_id}")
##mr = project.mergerequests.get(str(merge_request_id))
#if debug: print(f"DEBUG: Merge request={mr}")

mrs = project.mergerequests.list()
if (debug):
    print("DEBUG: MRs=")
    print(mrs)
for mr in mrs:
    print("DEBUG: MR:")
    print(mr)
    if (mr.id == int(merge_request_id)):
        print(f"DEBUG: Found MR #{merge_request_id}")
        break

changes = mr.changes()
if debug: print(f"DEBUG: mr.changes()={json.dumps(changes, indent=4)}")

file_changes = dict()
for change in changes['changes']:
    if debug: print(f"DEBUG: change={change}")
    filename = change['new_path']
    patch = change['diff']
    print(f"DEBUG: file={filename} patch={patch}")
    m = re.search('@@ .(\d+,\d+) .(\d+,\d+) @@', patch)
    if m:
        patch_changes = m.group(2)
        (start_line, num_lines) = patch_changes.split(',')
        end_line = int(start_line) + int(num_lines)
        if debug: print(f"DEBUG: Change start_line={start_line} num_lines={num_lines} end_line={end_line}")
        if filename not in file_changes:
            file_changes[filename] = []
        file_changes[filename].append({'start_line': start_line, 'end_line': end_line})

if debug: print(f"DEBUG: File changes={file_changes}")

# Get a list of all merge keys seen in analysis
seen_in_analysis = dict()
seen_in_comments = dict()

for issue in local_issues:
    seen_in_analysis[issue['mergeKey']] = 1

if debug: print(f"DEBUG: Discussions:")
for discussion in mr.discussions.list():
    if debug: print(f"DEBUG: discussion={discussion}")

    m = re.search('<!-- Coverity (.+?) -->', discussion.attributes['notes'][-1]['body'])
    if m:
        coverity_mk = m.group(1)
        seen_in_comments[coverity_mk] = 1
        if debug: print(f"DEBUG: Found Coverity comment for {coverity_mk}")
        if coverity_mk not in seen_in_analysis:
            print(f"DEBUG:  Not seen in analysis, resolving comment")

            discussion.resolved = True
            discussion.save()

for issue in issues_to_comment_on:
    if debug: print(f"DEBUG: Comment on issue {issue}")
    filename = issue['strippedMainEventFilePathname']

    if issue['checkerName'].startswith("SIGMA"):
        if debug: print(f"DEBUG: Checker name '{issue['checkerName']}' begins with SIGMA, skipping")
        continue

    if debug: print(f"DEBUG: Issue appears in {filename}")
    if filename not in file_changes:
        if debug: print(f"DEBUG: File '{filename}' not in change set, ignoring")
        continue

    if debug: print(f"DEBUG: File '{filename}' found in change set")

    if issue['mergeKey'] in seen_in_comments:
        if debug: print(f"DEBUG: Merge key {issue['mergeKey']} already seen in comments, do not create another comment")
        continue

    start_line = issue['mainEventLineNumber']

    events = issue['events']
    remediation = None
    main_desc = None
    for event in events:
        print(f"DEBUG: event={event}")
        if event['remediation'] == True:
            remediation = event['eventDescription']
        if event['main'] == True:
            main_desc = event['eventDescription']

    checkerProps = issue['checkerProperties']
    comment_body = f"**Coverity found issue: {checkerProps['subcategoryShortDescription']} - CWE-{checkerProps['cweCategory']}, {checkerProps['impact']} Severity**\n\n"

    if (main_desc):
        comment_body += f"**{issue['checkerName']}**: {main_desc} {checkerProps['subcategoryLocalEffect']}\n\n"
    else:
        comment_body += f"**{issue['checkerName']}**: {checkerProps['subcategoryLocalEffect']}\n\n"

    if remediation:
        comment_body += f"**How to fix:** {remediation}\n"

    comment_body += "<details>\n<summary>Click to expand data flow...</summary>\n\n"

    # Build map of lines
    event_tree_lines = dict()
    event_tree_events = dict()
    print("for event in events")
    for event in events:
        event_file = event['strippedFilePathname']
        event_line = int(event['lineNumber'])

        if event_file not in event_tree_lines:
            event_tree_lines[event_file] = dict()
            event_tree_events[event_file] = dict()

        event_line_start = event_line - 3
        if (event_line_start < 0): event_line_start = 0
        event_line_end = event_line + 3
        for i in range(event_line_start, event_line_end):
            event_tree_lines[event_file][i] = 1

        if event_line not in event_tree_events[event_file]:
            event_tree_events[event_file][event_line] = []

        event_tree_events[event_file][event_line].append(f"{event['eventNumber']}. {event['eventTag']}: {event['eventDescription']}")

    if debug: print(f"DEBUG: event_tree_lines={event_tree_lines}")
    if debug: print(f"DEBUG: event_tree_events={event_tree_events}")

    for filename in event_tree_lines.keys():
        comment_body += f"**From {filename}:**\n"

        comment_body += "```\n"
        for i in event_tree_lines[filename].keys():
            if (i in event_tree_events[filename]):
                for event_str in event_tree_events[filename][i]:
                    comment_body += f"{event_str}\n"

            code_line = linecache.getline(filename, i)
            comment_body += f"%5d {code_line}" % i

        comment_body += "```\n"

    comment_body += "</details>"

    # Tag with merge key
    comment_body += f"<!-- Coverity {issue['mergeKey']} -->"

    if debug: print(f"DEBUG: comment_body={comment_body}")

    # TODO Handle boundary violations better
    blame_ref = find_ref_for_line(filename, start_line).replace('^', '')
    if blame_ref == None:
        print(f"WARNING: Unable to find reference for {filename}:{start_line}, skipping")
        continue

    if debug: print(f"DEBUG: Reference for line={start_line} is: {blame_ref}")

    diffs = changes['diff_refs']
    # TODO Fix old_path - could it ever happen that it changes?
    mr.discussions.create({'body': comment_body,
                           'position': {
                               'base_sha': diffs['base_sha'],
                               'start_sha': diffs['start_sha'],
                               'head_sha': diffs['head_sha'],
                               'position_type': 'text',
                               'new_line': start_line,
                               'old_path': filename,
                               'new_path': filename}
                           })

    total_issues_commented += 1

# Process output from Sigma
with open(sigma_json) as f:
  data = json.load(f)

print(f"INFO: Reading incremental analysis results from {sigma_json}")
if(debug): print("DEBUG: " + json.dumps(data, indent = 4, sort_keys=True) + "\n")

# Get a list of all merge keys seen in analysis
seen_in_analysis = dict()
for issue in data['issues']['issues']:
    seen_in_analysis[issue['uuid']] = 1

if debug: print(f"DEBUG: Discussions:")
for discussion in mr.discussions.list():
    if debug: print(f"DEBUG: discussion={discussion}")

    m = re.search('<!-- Sigma (.+?) -->', discussion.attributes['notes'][-1]['body'])
    if m:
        sigma_uuid = m.group(1)
        seen_in_comments[sigma_uuid] = 1
        if debug: print(f"DEBUG: Found Sigma comment for {sigma_uuid}")
        if sigma_uuid not in seen_in_analysis:
            print(f"DEBUG:  Not seen in analysis, resolving comment")

            discussion.resolved = True
            discussion.save()

for issue in data['issues']['issues']:
    filename = issue['filepath']
    if filename not in file_changes:
        if debug: print(f"DEBUG: File '{filename}' not in change set, ignoring")
        continue

    if debug: print(f"DEBUG: File '{filename}' found in change set")

    if issue['uuid'] in seen_in_comments:
        if debug: print(f"DEBUG: UUID {issue['uuid']} already seen in comments, do not create another comment")
        continue

    cwe = "N/A"
    if "cwe" in issue['taxonomies']:
        cwe = issue['taxonomies']['cwe'][0]

    comment_body = f"**Coverity Rapid Scan found issue: {issue['summary']} - CWE-{cwe}, {issue['severity']['impact']} Severity**\n\n"
    comment_body += f"{issue['desc'].strip()}\n\n"

    comment_body += f"**How to fix:** {issue['remediation'].strip()}\n"

    issue_line = issue['location']['start']['line']

    if "fixes" in issue:
        fix = issue['fixes'][0]
        fix_desc = fix['desc']
        fix_action = fix['actions'][0]
        if debug: print(f"DEBUG: Fix action={fix_action}")

        fix_location_start_line = fix_action['location']['start']['line']
        fix_location_end_col = fix_action['location']['end']['column']
        fix_location_start_col = fix_action['location']['start']['column']
        code_line = linecache.getline(filename, fix_location_start_line)

        if debug: print(f"DEBUG: Original line: {code_line}")

        suggestion = code_line[0:fix_location_start_col-1] + fix_action['contents'] + code_line[fix_location_end_col-1:len(code_line)]

        if debug: print(f"DEBUG: Sugestion: {suggestion}")

        comment_body += f"```suggestion\n" \
                        f"{suggestion}" \
                        f"```\n"

    if debug: print(f"DEBUG: Comment body={comment_body}")

    blame_ref = find_ref_for_line(filename, start_line).replace('^', '')
    if blame_ref == None:
        print(f"WARNING: Unable to find reference for {filename}:{start_line}, skipping")
        continue

    if debug: print(f"DEBUG: Reference for line={start_line} is: {blame_ref}")

    diffs = changes['diff_refs']
    # TODO Fix old_path - could it ever happen that it changes?
    mr.discussions.create({'body': comment_body,
                           'position': {
                               'base_sha': diffs['base_sha'],
                               'start_sha': diffs['start_sha'],
                               'head_sha': diffs['head_sha'],
                               'position_type': 'text',
                               'new_line': issue_line,
                               'old_path': filename,
                               'new_path': filename}
                           })

# TODO break build